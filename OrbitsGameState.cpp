/****************************************************************************
*
* OrbitsGameState.cpp
* State that renders the orbit solver.
*
* This code copyright (c) 2012 Ellen Price <eprice@caltech.edu>
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. The name of the author may not be used to endorse or promote products
*    derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
****************************************************************************/

#include "OrbitsGameState.h"

#define PHYSICS_DELAY   (48)
#define RENDER_DELAY    (16)

#define TIMESTEP        (0.05)

#define MENU_LENGTH     (9)
#define RBUTTON_WIDTH   (150)
#define BUTTON_WIDTH    (100)
#define BUTTON_HEIGHT   (40)
#define BUTTON_HSPACE   (50)
#define BUTTON_SPACE    (50)
#define BUTTON_X        (10)
#define BUTTON_Y        (SCREEN_H - 30 - BUTTON_HEIGHT)

#define STATUS_X        (5)
#define STATUS_Y        (SCREEN_H - 20)
#define ENERGY_X        (5)
#define ENERGY_Y        (5)

#define SCALE					(200)
#define SUN_RADIUS              (0.2)
#define PLANET_RADIUS           (0.1)
#define PLANET_X_OFFSET         (SCREEN_W / 2)
#define PLANET_Y_OFFSET         (SCREEN_H / 2)
#define ECCENTRICITY			(0.2)

using namespace std;

extern GameState *top_state;

/*
 * Initializes the GameState
 */
OrbitsGameState::OrbitsGameState()
{
    /* Set initial state */
    method = METHOD_NONE;
    initialize_physics();

    /* Initialize the status message */
    strcpy(status, "Press F for forward Euler, B for backward Euler, and \
S for symplectic Euler");
}

/*
 * Deinitializes the GameState
 */
OrbitsGameState::~OrbitsGameState()
{

}

void OrbitsGameState::do_physics_proc()
{
    ((OrbitsGameState *) top_state)->physics_step();
}

void OrbitsGameState::do_render_proc()
{
    ((OrbitsGameState *) top_state)->render();
}

/*
 * Runs the GameState.
 */
StateStackCommand *OrbitsGameState::run()
{
    int ret = 2;

    install_int(OrbitsGameState::do_physics_proc, PHYSICS_DELAY);
    install_int(OrbitsGameState::do_render_proc, RENDER_DELAY);
    
    this->running = true;

    while(this->running)
    {
        this->process_input();
        
        rest(2);
    }

    remove_int(OrbitsGameState::do_physics_proc);
    remove_int(OrbitsGameState::do_render_proc);
    
    StateStackCommand *retc = new StateStackCommand();

    switch(ret)
    {
        case 2:
        default:
            retc->cmd = POP;
            retc->new_state = NULL;
    }

    return retc;
}

/*
 * Runs a step of the physics layer.
 */
void OrbitsGameState::physics_step()
{
    switch (method)
    {
        case METHOD_NONE:
            break;
            
        case METHOD_FORWARD_EULER:
            forward_euler(TIMESTEP);
            break;
            
        case METHOD_BACKWARD_EULER:
            backward_euler(TIMESTEP);
            break;
            
        case METHOD_SYMPLECTIC_EULER:
            symplectic_euler(TIMESTEP);
            break;
    }
    
    energy = 0.5 * (xvel * xvel + yvel * yvel) - 1. / sqrtf(x * x + y * y);
    sprintf(eng, "Energy: %f", energy);
}

/*
 * Processes incoming input, making it available to the physics layer.
 */
void OrbitsGameState::process_input()
{
    if (key[KEY_Q])
    {
        this->running = false;
    }
    else if (key[KEY_F])
    {
        initialize_physics();
        method = METHOD_FORWARD_EULER;
    }
    else if (key[KEY_B])
    {
        initialize_physics();
        method = METHOD_BACKWARD_EULER;
    }
    else if (key[KEY_S])
    {
        initialize_physics();
        method = METHOD_SYMPLECTIC_EULER;
    }
}

/*
 * Renders everything.
 */
void OrbitsGameState::render()
{
    start_render();
    textout_ex(doublebuf, font, status, STATUS_X, STATUS_Y, 
        makecol(255, 255, 255), -1);
    textout_ex(doublebuf, font, eng, ENERGY_X, ENERGY_Y,
        makecol(255, 255, 255), -1);
    circlefill(doublebuf, SCREEN_W / 2, SCREEN_H / 2, SUN_RADIUS * SCALE, 
        makecol(255, 255, 0));
    circlefill(doublebuf, x * SCALE + PLANET_X_OFFSET, y * SCALE + PLANET_Y_OFFSET, 
        PLANET_RADIUS * SCALE, makecol(0, 0, 255));
    end_render();
}

void OrbitsGameState::initialize_physics()
{
    /* Establish initial conditions before starting or re-starting
     * a simulation. */
    x = 1. - ECCENTRICITY;
    y = 0.;
    xvel = 0.;
    yvel = sqrtf((1. + ECCENTRICITY) / (1. - ECCENTRICITY));
}

void OrbitsGameState::forward_euler(float h)
{
	/* TODO: Write this function. It should implement a forward
	 * (or standard) Euler method. Given the initial value problem
	 *
	 *     x'(t) = f(t, x)
	 *     x''(t) = g(t, x)
	 *     x(0) = x0, x'(0) = v0
	 *
	 * we calculate the next step in the series by
	 *
	 *     v_(n+1) = v_n + h * g(t_n, x_n)
	 *     x_(n+1) = x_n + h * f(t_n, x_n)
	 *
	 * That is, we find the next step by calculating the derivative
	 * at the current step, multiply by some small constant h, and
	 * add to the current step. */
}

void OrbitsGameState::backward_euler(float h)
{
	/* TODO: Write this function. It should implement a backward
	 * Euler method. Given the initial value problem
	 *
	 *     x'(t) = f(t, x)
	 *     x''(t) = g(t, x)
	 *     x(0) = x0, x'(0) = v0
	 *
	 * we calculate the next step in the series by
	 *
	 *     v_(n+1) = v_n + h * g(t_(n+1), x_(n+1))
	 *     x_(n+1) = x_n + h * f(t_(n+1), x_(n+1))
	 *
	 * We find the next step by calculating the derivative
	 * at the next step, multiply by some small constant h, and
	 * add to the current step. */
}

void OrbitsGameState::symplectic_euler(float h)
{
    /* TODO: Write this function. It should implement a symplectic,
     * or self-implicit, Euler method. Given the initial value problem
     *
     *     x'(t) = f(t, x)
     *     x''(t) = g(t, x)
     *     x(0) = x0, x'(0) = v0
     *
     * we calculate the next step in the series by
     *
     *     v_(n+1) = v_n + h * g(t_n, x_n)
     *     x_(n+1) = x_n + h * f(t_n, v_(n+1))
     *
     * The only difference between the symplectic Euler and forward
     * Euler is that x_(n+1) is determined using v_(n+1) instead of
     * v_n. */
}
