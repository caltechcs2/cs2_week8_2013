/****************************************************************************
*
* GameState.cpp
* Abstract base class for the game states managed by the state stack.
*
* Original code copyright (c) 2012 Ben Yuan <byuan@caltech.edu>
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. The name of the author may not be used to endorse or promote products
*    derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
****************************************************************************/

#include <allegro.h>
#include "GameState.h"


/* Initializes the GameState.
 * This initializes all local variables.
 *
 * No arguments.
 *
 * No return values.
 * */
GameState::GameState() {
    // Sets up the double buffer.
    // We use a double buffer both to abstract away the underlying
    // Allegro screen, making the rendering more smooth, since the
    // program will then not be constantly attempting to draw
    // shapes to the display (which causes objects to flicker
    // under high load).
    
    // Buffers have a physical size,
    this->buf_hsize = SCREEN_W;
    this->buf_vsize = SCREEN_H;
    // and an origin point.
    // The origin point governs where the buffer is blitted
    // onto the Allegro display, allowing things like modal dialogs
    // to be rendered more easily.
    this->buf_xorig = 0;
    this->buf_yorig = 0;
    // Then we create a new bitmap for ourselves.
    this->doublebuf = create_bitmap(buf_hsize, buf_vsize);
}

/* Deinitializes the GameState.
 * This cleans up any explicitly allocated local variables.
 * 
 * No arguments.
 * 
 * No return values.
 * */ 
GameState::~GameState() {
    // Destroys the double buffer.
    destroy_bitmap(this->doublebuf);
}

/* Prepares to render a frame.
 * Call this at the beginning of every frame rendering operation.
 * 
 * No arguments.
 * 
 * No return values.
 * */ 
void GameState::start_render() {
    // Clears the double buffer for rendering,
    clear_bitmap(this->doublebuf);
    // and hides the mouse.
    scare_mouse();
}

/* Commits a rendering operation.
 * Call this once done rendering to the double buffer.
 * 
 * No arguments.
 * 
 * No return values.
 * */ 
void GameState::end_render() {
    // Commits the double buffer to the main screen,
    acquire_screen();
    blit(this->doublebuf, screen, 0, 0, buf_xorig, buf_yorig, buf_hsize, buf_vsize);
    release_screen();
    // and shows the mouse.
    unscare_mouse();
}

/* Custom button proc that intercepts a D_CLOSE and calls a function in dp3. 
 * 
 * This proc is intended to be used in a menu.
 * By default, buttons in menus try to close the menu when pressed.
 * This function allows custom button behavior to be defined.
 * 
 * The function in the dp3 field of the DIALOG object
 * (which is really a dialog _element_)
 * must take no arguments and must return void.
 * 
 * Arguments:
 *  msg: int
 *      a message from the dialog manager to this button,
 *      telling it what to do.
 *  d: DIALOG *
 *      pointer to the dialog element which contains this custom button.
 *      (Buttons are functions. Odd, I know, but that's how Allegro 4 does it.)
 *  c: int
 *      a character code corresponding to a pressed key;
 *      not really used by buttons.
 * 
 * Return value: int
 *      a special Allegro message to the dialog manager,
 *      telling it what to do.
 * 
 * */
int GameState::custom_button_proc(int msg, DIALOG *d, int c)
{
   // Pretend we're a button for the time being, but intercept its return message.
   int ret = d_button_proc(msg, d, c);
   // If we're trying to close the window, but we really want to do something else,
   // then do something else.
   if (ret == D_CLOSE && d->dp3)
      return ((int (*)(void))d->dp3)();
   // In any case, the dialog manager wants to know what to do next,
   // so we will tell it.
   return ret;
}
