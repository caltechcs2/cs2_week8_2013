/****************************************************************************
*
* main.cpp
* The main game metaloop.
*
* Original code copyright (c) 2012 Ben Yuan <byuan@caltech.edu>
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. The name of the author may not be used to endorse or promote products
*    derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
****************************************************************************/

#include "main.h"

/* This is a global pointer that always points to the top state.
 * This exists to allow states to expose meaningful function pointers
 * to the Allegro 4 routines (e.g. timer routines). */
GameState * top_state;

using namespace std;

/*
 * Each game state represents a 'mini-application' with its own logic and
 * rendering routines, possibly (though not necessarily) independent of
 * any of the other states. This makes implementing menus, modal windows,
 * multiple game modes, and the like very easy to implement: each one is
 * simply its own state.
 */
int main() {
    /* Initialize the Allegro library. */
    allegro_init();

    /* Set the graphics mode here. */
    set_color_depth(32);    
    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Unable to set graphic mode\n%s\n", allegro_error);
        return 1;
    }
    
    /* Install the necessary peripheral handlers here. */
    install_keyboard();
    install_mouse();
    install_timer();
    
    /* Display the mouse cursor. */
    show_mouse(screen);
    
    /* Initalize the state stack. */
    stack<GameState *> states;
    
    /* Create a new default state and push it onto the stack.
     * Since we have a main menu, we'll start there. */
    MainMenuGameState * s = new MainMenuGameState();
    states.push(s);
    
    /* The main game loop. */
    while(!states.empty()) {
        /* Set the global pointer to the top state. */
        top_state = states.top();
        
        /* Run the top state. */
        StateStackCommand * return_command = top_state->run();

        if (return_command->cmd == QUIT_NOW)
        {
            break;
        }
        else if (return_command->cmd == POP)
        {
            states.pop();
            delete top_state;
        }
        else if (return_command->cmd == PUSH)
        {
            states.push((return_command->new_state));
        }

        delete return_command;
    }
    
    /* At this point, we have either QUIT_NOW or popped the last remaining state.
     * Time to clean up. */

    allegro_exit();
    return 0;
}
END_OF_MAIN()
