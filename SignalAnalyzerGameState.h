/****************************************************************************
*
* SignalAnalyzerGameState.h
* State that renders the signal analyzer.
*
* Original code copyright (c) 2012 Ellen Price <eprice@caltech.edu>
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. The name of the author may not be used to endorse or promote products
*    derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
****************************************************************************/

#ifndef SIGNAL_ANALYZER_GAME_STATE_H
#define SIGNAL_ANALYZER_GAME_STATE_H

#include <allegro.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "loadpng.h"
#include <png.h>
#include <zlib.h>
#include "GameState.h"
#include "StateStack.h"

#define PI          (3.14159265358979)
#define NUM_SAMPLES (512)
#define MAX_PATH    (200)

using namespace std;

typedef enum
{
    CONTENTS_NONE,
    CONTENTS_RAWDATA,
    CONTENTS_FFT,
    CONTENTS_IFFT,
} SignalAnalyzerDataContents;

typedef struct
{
    char sig_riff[4];
    char len[4];
    char sig_wave[4];
} riff_hdr;

typedef struct
{
    char sig[4];
    char len[4];
    char tag[2];
    char channels[2];
    char sample_rate[4];
    char byte_rate[4];
    char block_align[2];
    char bits_per_sample[2];
} fmt_chunk;

typedef struct
{
    char sig[4];
    char len[4];
} data_chunk;

class SignalAnalyzerGameState : public GameState
{
public:
	SignalAnalyzerGameState();
    ~SignalAnalyzerGameState();
    
    virtual StateStackCommand *run();
    
    static void do_physics_proc();
    static void do_render_proc();
    
protected:
    virtual void process_input();    
    virtual void render();    
    virtual void physics_step();
    
    void construct_menu();
    int read_wav();
    int draw_signal();
    int draw_fft();
    int highpass();
    int draw_ifft();    
    static int mem_fun_button_proc(int msg, DIALOG *d, int c);
    
    /* Student-implemented functions */    
    void fft(float *data, unsigned long nn, int isign);
    void highpass_filter(float *data, float *out);
    void window(float *d);
    
private:
    BITMAP *bmp;
    DIALOG *dlg;
    float *d, *sig;
    char status[200];
    SignalAnalyzerDataContents contents;  
    unsigned long sample_rate;
};

#endif
