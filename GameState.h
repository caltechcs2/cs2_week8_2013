/****************************************************************************
*
* GameState.H
* Abstract base class for the game states managed by the state stack.
*
* Original code copyright (c) 2012 Ben Yuan <byuan@caltech.edu>
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. The name of the author may not be used to endorse or promote products
*    derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
****************************************************************************/

#ifndef __GAMESTATE_H__
#define __GAMESTATE_H__

#include <allegro.h>
#include "StateStack.h"

// prototype for state stack command
struct StateStackCommand;

/*
 * This is the base class for all GameStates.
 * 
 * A GameState defines a complete main loop for a portion of an Allegro program,
 * including instructions to run logic, draw to the screen, and clean up after
 * finishing.
 * 
 * Define a new GameState for each major behavior exhibited by your program.
 * For example, a game might have separate states for a main menu, a game in
 * progress, a save-game window, a load-game window, and a high-scores window.
 * 
 * To define what the state should do when asked to run, overload the run()
 * function. The specifics are completely up to the derived classes. In general,
 * run() should set up a main loop that handles rendering and state updates,
 * or should set up timer callbacks to allow the two to run independently.
 * 
 */
class GameState {

public:
    GameState();
    virtual ~GameState();
    
    static int custom_button_proc(int, DIALOG *, int);
    
    virtual StateStackCommand * run() = 0;
    
protected:
    BITMAP * doublebuf;
    int buf_hsize, buf_vsize;
    int buf_xorig, buf_yorig;

    virtual void process_input() = 0;
    
    virtual void render() = 0;
    virtual void start_render();
    virtual void end_render();
    
};

#endif
