/****************************************************************************
*
* MainMenuGameState.cpp
* State that renders a main menu.
*
* Original code copyright (c) 2012 Ben Yuan <byuan@caltech.edu>
* Modified by Ellen Price <eprice@caltech.edu>
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. The name of the author may not be used to endorse or promote products
*    derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
****************************************************************************/

#include <allegro.h>
#include "MainMenuGameState.h"
#include "SignalAnalyzerGameState.h"
#include "OrbitsGameState.h"

#define MAINMENU_LENGTH     (7)

/* Initializes the MainMenuGameState.
 * This initializes all local variables.
 * */
 
MainMenuGameState::MainMenuGameState()
{
    // Temporary pointer to prevent us having to type out
    // this->main_menu_dialog[n] each time.
    DIALOG * t = NULL;
    int n = 0;
    
    // Initialize the DIALOG array.
    this->main_menu_dialog = new DIALOG[MAINMENU_LENGTH];
    // Construct the DIALOG array.
    t = &(this->main_menu_dialog[n++]);
    t->proc = d_clear_proc;
    t->x = 0;   t->y = 0;   t->w = 0;   t->h = 0;
    t->fg = 0;      t->bg = 0;      t->key = 0;
    t->flags = 0;
    t->d1 = 0;      t->d2 = 0;
    t->dp = NULL;
    t->dp2 = NULL;
    t->dp3 = NULL;
    
    this->construct_menu(n);
    
    t = &(this->main_menu_dialog[n++]);
    t->proc = d_yield_proc;
    t->x = 0;   t->y = 0;   t->w = 0;   t->h = 0;
    t->fg = 0;      t->bg = 0;      t->key = 0;
    t->flags = 0;
    t->d1 = 0;      t->d2 = 0;
    t->dp = NULL;
    t->dp2 = NULL;
    t->dp3 = NULL;
    
    t = &(this->main_menu_dialog[n++]);
    t->proc = NULL;
    t->x = 0;   t->y = 0;   t->w = 0;   t->h = 0;
    t->fg = 0;      t->bg = 0;      t->key = 0;
    t->flags = 0;
    t->d1 = 0;      t->d2 = 0;
    t->dp = NULL;
    t->dp2 = NULL;
    t->dp3 = NULL;
    
    // Sets default colors.
    int gui_fg_color = makecol(0,0,0);
    int gui_bg_color = makecol(200,240,200);
    set_dialog_color(this->main_menu_dialog, gui_fg_color, gui_bg_color);
    
    // white color for d_clear_proc and the d_?text_procs
    this->main_menu_dialog[0].bg = makecol(255, 255, 255);
    for (int i = 1; this->main_menu_dialog[i].proc; i++) {
        if (this->main_menu_dialog[i].proc == d_text_proc ||
            this->main_menu_dialog[i].proc == d_ctext_proc ||
            this->main_menu_dialog[i].proc == d_rtext_proc)
        {
            this->main_menu_dialog[i].bg = this->main_menu_dialog[0].bg;
        }
    }
    
    position_dialog(this->main_menu_dialog, 0, 0);
    
}

/* Deinitializes the GameState.
 * This cleans up any explicitly allocated local variables.
 * */ 
MainMenuGameState::~MainMenuGameState()
{
    delete this->main_menu_dialog;
}

/* Runs the GameState.
 * This runs any necessary render, physics, and processing loops.
 * When finished, constructs a command for the stack to execute,
 * including a new GameState if necessary.
 * */
StateStackCommand * MainMenuGameState::run() {
    // Clear the screen (to avoid artifacts).
    this->start_render();
    this->render();
    this->end_render();
    
    // Run the dialog.
    int ret = do_dialog(this->main_menu_dialog, -1);
    
    // Construct a new state to return based on
    // the return value of do_dialog.
    // Observe that do_dialog returns the index of the
    // element that caused it to return.
    
    StateStackCommand* retc = new StateStackCommand();
    
    switch(ret) {
        case 2: // the first button 
            retc->cmd = PUSH;
            retc->new_state = (GameState *) new SignalAnalyzerGameState();
            break;
        case 3: // the second button
            retc->cmd = PUSH;
            retc->new_state = (GameState *) new OrbitsGameState();
            break;
        case 4: // the quit button
        default:
            retc->cmd = POP;
            retc->new_state = NULL;
            break;
    }
    
    return retc;
    
}

/* Because this state is pure dialogness, this function need not do anything.
 * */
void MainMenuGameState::process_input() {
    
}

/* Because this state is pure dialogness, this function need only clear the screen
 * to prevent graphics artifacts.
 * */
void MainMenuGameState::render() {
    clear_to_color(this->doublebuf, makecol(255,255,255));
}

/* Subroutine that constructs the menu.
 * We broke this off of the main region to allow subclasses to
 * override its contents more easily.
 * 
 * Observe that dialog element indices start from 1,
 * since 0 was defined as a d_clear_proc.
 */
 
void MainMenuGameState::construct_menu(int & n)
{
    DIALOG * t;    
    
    t = &(this->main_menu_dialog[n++]);
    t->proc = d_ctext_proc;
    t->x = SCREEN_W/2;   t->y = 20;   t->w = 0;   t->h = 0;
    t->fg = 0;      t->bg = 0;      t->key = 0;
    t->flags = 0;
    t->d1 = 0;      t->d2 = 0;
    t->dp = (void*)"Main Menu";
    t->dp2 = NULL;
    t->dp3 = NULL;
    
    t = &(this->main_menu_dialog[n++]);
    t->proc = d_button_proc;
    t->x = SCREEN_W/2-125;   t->y = 60;   t->w = 250;   t->h = 40;
    t->fg = 0;      t->bg = 0;      t->key = 0;
    t->flags = D_EXIT;
    t->d1 = 0;      t->d2 = 0;
    t->dp = (void*)"Signal Analyzer";
    t->dp2 = NULL;
    t->dp3 = NULL;
    
    t = &(this->main_menu_dialog[n++]);
    t->proc = d_button_proc;
    t->x = SCREEN_W/2-125;   t->y = 120;   t->w = 250;   t->h = 40;
    t->fg = 0;      t->bg = 0;      t->key = 0;
    t->flags = D_EXIT;
    t->d1 = 0;      t->d2 = 0;
    t->dp = (void*)"Orbits";
    t->dp2 = NULL;
    t->dp3 = NULL;
    
    t = &(this->main_menu_dialog[n++]);
    t->proc = d_button_proc;
    t->x = SCREEN_W/2-125;   t->y = 180;   t->w = 250;   t->h = 40;
    t->fg = 0;      t->bg = 0;      t->key = 0;
    t->flags = D_EXIT;
    t->d1 = 0;      t->d2 = 0;
    t->dp = (void*)"Quit";
    t->dp2 = NULL;
    t->dp3 = NULL;
}

